//Http error codes
export const BAD_REQUEST = 'Bad Request';//400
export const NOT_FOUND = 'Not Found';//404
export const SERVER_ERROR = 'Internal Server Error';//500
//Jwt error codes
export const TOKEN_EXPIRED = 'jwt expired';
export const TOKEN_MALFORMED = 'jwt malformed';
export const SIGNATURE_REQUIRED = 'jwt signature is required';
export const INVALID_SIGNATURE = 'invalid signature';
export const AUDIENCE_INVALID = 'jwt audience invalid. expected: [OPTIONS AUDIENCE]';
export const ISSUER_INVALID = 'jwt issuer invalid. expected: [OPTIONS ISSUER]';
export const ID_INVALID = 'jwt id invalid. expected: [OPTIONS JWT ID]';
export const SUBJECT_INVALID = 'jwt subject invalid. expected: [OPTIONS SUBJECT]';
export const NOT_ACTIVE = 'jwt not active';
