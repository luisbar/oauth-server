export const ACCESS_TOKEN_TTL = 3600;
export const REFRESH_TOKEN_TTL = 7200;
export const TOKEN_TYPE = 'bearer';
export const TOKEN_ISSUER = 'localhost:3000';

export const HANDLERS_NOT_RESTRICTED = [
  'authorization:authorize',
  'user:signIn',
];
