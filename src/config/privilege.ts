import { ReflectMetadata } from '@nestjs/common';

export const Privilege = (privilege: string) => ReflectMetadata('privilege', privilege);
