import { Module, ValidationPipe } from '@nestjs/common';
import { APP_PIPE, APP_FILTER, APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { ExceptionHandler } from './common/exception.handler';
import { RequestValidator } from './common/request.validator';
import { UserModule } from './api/user/user.module';
import { AuthorizationModule } from './api/authorization/authorization.module';
import { ScopeModule } from './api/scope/scope.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    AuthorizationModule,
    ScopeModule
  ],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
     provide: APP_FILTER,
     useClass: ExceptionHandler,
    },
    {
     provide: APP_GUARD,
     useClass: RequestValidator,
    },
  ],
})

export class AppModule {
  constructor(private readonly connection: Connection) {}
}
