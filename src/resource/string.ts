//ExceptionHandler
const TXT_1 = 'Hay datos erroneos';
const TXT_2 = 'Lo sentimos, el recurso no existe';
const TXT_3 = 'Algo malo sucedio, en un momento nuestros programadores lo solucionaran :)';
const TXT_4 = 'Algo malo sucedio, lo sentimos :(';
//Jwt
const TXT_5 = 'El token ha expirado';
const TXT_6 = 'El token esta mal formado';
const TXT_7 = 'El token no contiene la firma';
const TXT_8 = 'La firma del token es inválida';
const TXT_9 = 'La audiencia del token es inválida';
const TXT_10 = 'El emisor del token es inválido';
const TXT_11 = 'El id del token es inválido';
const TXT_12 = 'El asunto del token es inválido';
const TXT_13 = 'El token aún no está activo';
//Field errors
const TXT_14 = 'Este campo no puede estar vacío';
const TXT_15 = 'Este campo debería contener un string';
const TXT_16 = 'Este campo no debe tener mas de $constraint1 caracteres';
const TXT_17 = 'Este campo no debe tener menos de $constraint1 caracteres';
const TXT_18 = 'El usuario debería contener al menos un privilegio';

export {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
  TXT_14,
  TXT_15,
  TXT_16,
  TXT_17,
  TXT_18,
};
