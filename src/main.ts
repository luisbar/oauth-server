import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useStaticAssets('src/resource');
  app.setBaseViewsDir('src/resource/template');
  app.setViewEngine('hbs');

  app.enableCors();

  await app.listen(3001);
}

bootstrap();
