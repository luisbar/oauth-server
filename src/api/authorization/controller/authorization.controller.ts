import { Controller, Get, Query, Render } from '@nestjs/common';
const debug = require('debug')('controller:authorization');

import { Privilege } from '../../../config/privilege';

@Controller()
export class AuthorizationController {

  @Get('authorize')
  @Render('login')
  @Privilege('authorization:authorize')
  authorize(@Query() query) {
    const {
      response_type,
      scope,
      client_id,
      state,
      redirect_uri
    } = query;

    debug(query);
    //TODO: check if the response_type is equal token or code
    //TODO: check if scope is valid
    //TODO: check if the client_id is matched with the client_id in database
    //TODO: check if the redirect_uri is matched with the redirect_uri in database

    return { response_type, scope, client_id, state, redirect_uri };
  }
}
