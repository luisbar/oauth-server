import { Module } from '@nestjs/common';
import { AuthorizationController } from './controller/authorization.controller';

@Module({
  controllers: [AuthorizationController]
})

export class AuthorizationModule {}
