import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Scope {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 30, nullable: false })
  controller: string;

  @Column({ length: 30, nullable: false })
  handler: string;
}
