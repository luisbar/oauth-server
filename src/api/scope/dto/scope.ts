import { IsString, MaxLength, MinLength } from 'class-validator';

import {
  TXT_15,
  TXT_16,
  TXT_17,
} from '../../../resource/string';

export class Scope {

  readonly id: number;

  @IsString({ message: TXT_15 })
  @MaxLength(30, { message: TXT_16 })
  @MinLength(3, { message: TXT_17 })
  readonly controller: string;

  @IsString({ message: TXT_15 })
  @MaxLength(30, { message: TXT_16 })
  @MinLength(3, { message: TXT_17 })
  readonly handler: string;
}
