import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ScopeController } from './controller/scope.controller';
import { ScopeService } from './service/scope.service';
import { Scope } from './entity/scope';

@Module({
  imports: [TypeOrmModule.forFeature([Scope])],
  controllers: [ScopeController],
  providers: [ScopeService]
})

export class ScopeModule {}
