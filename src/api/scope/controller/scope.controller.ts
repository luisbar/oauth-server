import { Controller, Post, Get, Body, Param, NotFoundException } from '@nestjs/common';

import { Scope } from '../dto/scope';
import { ScopeService } from '../service/scope.service';
import { Privilege } from '../../../config/privilege';

@Controller('scope')
export class ScopeController {

  constructor(private readonly scopeService: ScopeService) {}

  @Post('create')
  @Privilege('scope:create')
  async create(@Body() scope: Scope): Promise<Scope> {
    const scopeCreated = await this.scopeService.create(scope);
    return scopeCreated;
  }

  @Get('delete/:id')
  @Privilege('scope:delete')
  async delete(@Param('id') id: number): Promise<Scope> {
    const scopeMatched = await this.scopeService.findById(id);

    if (!Object.keys(scopeMatched).length)
      throw new NotFoundException('El scope no existe');

    const scopeDeleted = await this.scopeService.delete(scopeMatched);
    return scopeDeleted;
  }

  @Get('find')
  @Privilege('scope:findAll')
  async findAll(): Promise<Scope[]> {
    const scopes = await this.scopeService.findAll();
    return scopes;
  }
}
