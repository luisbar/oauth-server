import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Scope } from '../entity/scope';

@Injectable()
export class ScopeService {

  constructor(
    @InjectRepository(Scope)
    private readonly scopeRepository: Repository<Scope>,
  ) {}

  async create(scope: Scope): Promise<Scope> {
    try {
      return await this.scopeRepository.save(scope);
    } catch(exception) {
      throw new InternalServerErrorException('Error al guardar el scope');
    }
  }

  delete(scope: Scope): Promise<Scope> {
    return this.scopeRepository.remove(scope);
  }

  findAll(): Promise<Scope[]> {
    return this.scopeRepository.find();
  }

  async findById(id: number): Promise<any> {
    return  await this.scopeRepository.findOne({ id }) || {};
  }
}
