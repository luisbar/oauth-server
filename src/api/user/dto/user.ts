import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

import {
  TXT_14,
  TXT_15,
  TXT_16,
  TXT_17,
} from '../../../resource/string';

export class User {

  readonly id: number;

  @IsNotEmpty({ message: TXT_14 })
  @IsString({ message: TXT_15 })
  @MaxLength(30, { message: TXT_16 })
  readonly username: string;

  @IsString({ message: TXT_15 })
  @MaxLength(30, { message: TXT_16 })
  @MinLength(8, { message: TXT_17 })
  readonly password: string;

  readonly tokens: any;

  readonly scopes: any;
}
