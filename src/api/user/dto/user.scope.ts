import { ArrayNotEmpty } from 'class-validator';

import { TXT_18 } from '../../../resource/string';
import { Scope } from '../../scope/dto/scope';
import { User } from './user';

export class UserScope extends User {

  @ArrayNotEmpty({ message: TXT_18 })
  readonly scopes: Scope[];
}
