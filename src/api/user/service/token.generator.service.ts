import { Injectable, InternalServerErrorException } from '@nestjs/common';
const jwt = require('jsonwebtoken');
const debug = require('debug')('service:token.generator');
const uuidv4 = require('uuid/v4');

import { JWT_SECRET } from '../../../config/env';
import {
  ACCESS_TOKEN_TTL,
  REFRESH_TOKEN_TTL,
  TOKEN_ISSUER,
} from '../../../config/var';

@Injectable()
export class TokenGeneratorService {

  private payload: object;
  private jwtid: string;
  private accessTokenOptions: object;
  private refreshTokenOptions: object;

  constructor() {
    this.payload = {};
    this.accessTokenOptions = {
      algorithm: 'HS256',
      expiresIn: ACCESS_TOKEN_TTL,
      notBefore: 0,
      issuer: TOKEN_ISSUER,
    };
    this.refreshTokenOptions = {
      algorithm: 'HS256',
      expiresIn: REFRESH_TOKEN_TTL,
      notBefore: ACCESS_TOKEN_TTL,
    };
  }

  generateAccessToken(audience, scope): string {
    this.jwtid = uuidv4();
    return jwt.sign({ ...this.payload, scope }, JWT_SECRET, { ...this.accessTokenOptions, audience, jwtid: this.jwtid });
  }

  generateRefreshToken(): string {
    return jwt.sign(this.payload, JWT_SECRET, this.refreshTokenOptions);
  }

  verifyToken(token): any {
    return jwt.verify(token, JWT_SECRET, this.tokenVerified);
  }

  tokenVerified = (error, tokenDecoded) => {
    if (error) throw new InternalServerErrorException(null, error.message);

    debug(tokenDecoded);
    return tokenDecoded;
  }

  decodeAccessToken(token: string): any {
    return jwt.decode(token);
  }

  getJwtid(): string {
    return this.jwtid;
  }
}
