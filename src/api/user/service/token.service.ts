import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Token } from '../entity/token';

@Injectable()
export class TokenService {

  constructor(
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
  ) {}

  async createOrUpdate(token: Token): Promise<Token> {
    try {
      let tokenMatched = await this.tokenRepository.findOne({ cliendId: token.cliendId, user: token.user });

      if (tokenMatched) {
        tokenMatched.tokenId = token.tokenId;
        return await this.tokenRepository.save(tokenMatched);
      } else
        return await this.tokenRepository.save(token);
    } catch(exception) {
      throw new InternalServerErrorException('Error al guardar el token en la token');
    }
  }

  delete(token: Token): Promise<Token> {
    return this.tokenRepository.remove(token);
  }

  async findByTokenId(tokenId: string): Promise<Token> {
    return  await this.tokenRepository.findOne({ tokenId });
  }
}
