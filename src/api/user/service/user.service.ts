import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from '../entity/user';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async createOrUpdate(user: User): Promise<User> {
    try {
      return await this.userRepository.save(user);
    } catch(exception) {
      throw new InternalServerErrorException('Error al guardar el usuario');
    }
  }

  delete(user: User): Promise<User> {
    return this.userRepository.remove(user);
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  async findById(id: number): Promise<any> {
    return  await this.userRepository.findOne({ id }) || {};
  }

  findByUsernameAndPassword(username: string, password: string): Promise<User> {
    return this.userRepository.findOne({ username, password });
  }
}
