import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserController } from './controller/user.controller';
import { TokenGeneratorService } from './service/token.generator.service';
import { UserService } from './service/user.service';
import { TokenService } from './service/token.service';
import { User } from './entity/user';
import { Token } from './entity/token';

@Module({
  imports: [TypeOrmModule.forFeature([User, Token])],
  controllers: [UserController],
  providers: [TokenGeneratorService, UserService, TokenService],
  exports: [TokenGeneratorService, TokenService]
})

export class UserModule {}
