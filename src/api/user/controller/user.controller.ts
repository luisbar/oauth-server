import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Patch,
  Delete,
  Query,
  Res,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common';

import { TokenGeneratorService } from '../service/token.generator.service';
import { UserService } from '../service/user.service';
import { TokenService } from '../service/token.service';
import { User } from '../dto/user';
import { UserScope } from '../dto/user.scope';
import { Token } from '../entity/token';
import { Scope } from '../../scope/entity/scope';
import { Privilege } from '../../../config/privilege';

@Controller('user')
export class UserController {

  constructor(
    private readonly tokenGeneratorService: TokenGeneratorService,
    private readonly userService: UserService,
    private readonly tokenService: TokenService,
  ) {}

  @Post('upsert')
  @Privilege('user:upsert')
  async createOrUpdate(@Body() user: UserScope): Promise<User> {
    const userCreatedOrUpdated = await this.userService.createOrUpdate(user);
    return userCreatedOrUpdated;
  }

  @Get('delete/:id')
  @Privilege('user:delete')
  async delete(@Param('id') id: number): Promise<User> {
    const userMatched = await this.userService.findById(id);

    if (!Object.keys(userMatched).length)
      throw new NotFoundException('El usuario no existe');

    const userDeleted = await this.userService.delete(userMatched);
    return userDeleted;
  }

  @Get('find')
  @Privilege('user:findAll')
  async findAll(): Promise<User[]> {
    const users = await this.userService.findAll();
    return users;
  }

  @Get('find/:id')
  @Privilege('user:findById')
  async findById(@Param('id') id: number): Promise<User> {
    const userMatched = await this.userService.findById(id);
    return userMatched;
  }

  @Post('signIn')
  @Privilege('user:signIn')
  async signIn(@Body() user: User, @Query() query, @Res() res) {
    const userMatched = await this.checkIfUsernameAndPasswordMatched(user);
    const response_type = query.response_type;
    const scope = query.scope;
    const client_id = query.client_id;
    const state = query.state;
    const accessToken = this.addTokenToWhitelist(userMatched, client_id, scope);
    let redirect_uri = query.redirect_uri;

    redirect_uri += `?token=${accessToken}&state=${state}`;
    res.redirect(redirect_uri);
  }

  async checkIfUsernameAndPasswordMatched(user: User) {
    const userMatched = await this.userService.findByUsernameAndPassword(
      user.username,
      user.password
    );

    if (!userMatched)
      throw new UnauthorizedException('Nombre de usuario y/o contraseña incorrecto(s)');

    return userMatched;
  }

  addTokenToWhitelist(userMatched: User, client_id: string, scope: string): string {
    let accessToken = this.tokenGeneratorService.generateAccessToken(client_id, scope);
    let token = new Token();

    token.user = userMatched;
    token.cliendId = client_id;
    token.tokenId = this.tokenGeneratorService.getJwtid();

    this.tokenService.createOrUpdate(token);

    return accessToken;
  }

  @Get('revokeToken/:accessToken')
  @Privilege('user:revokeToken')
  async revokeToken(@Param('accessToken') accessToken: string) {
    const payload = this.tokenGeneratorService.decodeAccessToken(accessToken);
    const tokenMatched = await this.tokenService.findByTokenId(payload.jti);

    if (!tokenMatched)
      throw new NotFoundException('El token no existe');

    return await this.tokenService.delete(tokenMatched);
  }
}
