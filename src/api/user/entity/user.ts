import { Entity, Column, PrimaryGeneratedColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';

import { Token } from './token';
import { Scope } from '../../scope/entity/scope';

@Entity()
export class User {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 30, unique: true, nullable: false })
  username: string;

  @Column({ length: 30, nullable: false, select: false })
  password: string;

  @OneToMany(type => Token, token => token.user)
  tokens: Token[];

  @ManyToMany(type => Scope)
  @JoinTable({ name: 'user_scope' })
  scopes: Scope[];
}
