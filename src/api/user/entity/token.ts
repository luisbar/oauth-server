import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { User } from './user';

@Entity()
export class Token {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User, user => user.tokens)
  user: User;

  @Column({ nullable: false })
  cliendId: string;

  @Column({ nullable: false })
  tokenId: string;
}
