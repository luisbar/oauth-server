import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
const debug = require('debug')('guard:request.validator');

import { TokenGeneratorService } from '../api/user/service/token.generator.service';
import { TokenService } from '../api/user/service/token.service';
import { HANDLERS_NOT_RESTRICTED } from '../config/var';

@Injectable()
export class RequestValidator implements CanActivate {

  constructor(
    private readonly tokenGeneratorService: TokenGeneratorService,
    private readonly reflector: Reflector,
    private readonly tokenService: TokenService,
  ) {}

  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const privilegeRequested = this.reflector.get<string>('privilege', context.getHandler());

    debug(privilegeRequested);

    if (HANDLERS_NOT_RESTRICTED.indexOf(privilegeRequested) === -1) {

      const request = context.switchToHttp().getRequest();
      const authorization = request.headers.authorization;
      const authorizationSplitted = authorization ? authorization.split(' ') : [];
      const bearer = authorizationSplitted.length ? authorizationSplitted[0] : '';
      const token = authorizationSplitted.length === 2 ? authorizationSplitted[1] : '';
      const payload = this.tokenGeneratorService.verifyToken(token);
      const scope = payload.scope.split(' ');
      const allowed = scope.some((privilege) => privilege === privilegeRequested);
      const isTokenInWhitelist = await this.tokenService.findByTokenId(payload.jti);

      debug(allowed);
      debug(isTokenInWhitelist);

      if (bearer === 'Bearer' && allowed && isTokenInWhitelist)
        return true;
      else
        return false;
    }

    return true
  }
}
