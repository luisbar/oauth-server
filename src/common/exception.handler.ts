import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
const debug = require('debug')('filter:exception.handler');

import {
  BAD_REQUEST,
  NOT_FOUND,
  SERVER_ERROR,
  TOKEN_EXPIRED,
  TOKEN_MALFORMED,
  SIGNATURE_REQUIRED,
  INVALID_SIGNATURE,
  AUDIENCE_INVALID,
  ISSUER_INVALID,
  ID_INVALID,
  SUBJECT_INVALID,
  NOT_ACTIVE,
} from '../config/error.codes';
import {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
} from '../resource/string';

@Catch(HttpException)
export class ExceptionHandler implements ExceptionFilter {

  private httpContext: any;
  private httpException: any;
  private response: any;
  private request: any;
  private statusCode: any;
  private errorName: any;
  private message: any;
  private fieldMessages: any;
  private json: any;

  catch(httpException: HttpException, argumentsHost: ArgumentsHost) {
    this.setHttpExceptionMessageFromHttpException(httpException);
    this.setHttpContextFromArgumentsHost(argumentsHost);
    this.setResponse();
    this.setRequest();
    this.setStatusCode();
    this.setErrorName();
    this.setMessageAndFieldMessages();
    this.setJson();

    debug(JSON.stringify(httpException.message, null, 2));
    debug(JSON.stringify(this.json, null, 2));

    this.response
    .status(this.statusCode)
    .json(this.json);
  }

  setHttpContextFromArgumentsHost(argumentsHost) {
    this.httpContext = argumentsHost.switchToHttp();
  }

  setHttpExceptionMessageFromHttpException(httpException) {
    this.httpException = httpException.message;
  }

  setResponse() {
    this.response = this.httpContext.getResponse();
  }

  setRequest() {
    this.request = this.httpContext.getRequest();
  }

  setStatusCode() {
    this.statusCode = this.httpException.statusCode;
  }

  setErrorName() {
    this.errorName = this.httpException.error;
  }

  setMessageAndFieldMessages() {
    this.message = this.getMessage();
    this.fieldMessages = this.getFieldMessages();
  }

  setJson() {
    this.json = {
      statusCode: this.statusCode,
      errorName: this.errorName,
      message: this.message,
      fieldMessages: this.fieldMessages,
      path: this.request.url,
      timestamp: new Date().toISOString(),
    };
  }

  getMessage() {
    switch (this.errorName) {

      case BAD_REQUEST:
        return TXT_1;

      case NOT_FOUND:
        return TXT_2;

      case SERVER_ERROR:
        return TXT_3;

      case TOKEN_EXPIRED:
        return TXT_5;

      case TOKEN_MALFORMED:
        return TXT_6;

      case SIGNATURE_REQUIRED:
        return TXT_7;

      case INVALID_SIGNATURE:
        return TXT_8;

      case AUDIENCE_INVALID:
        return TXT_9;

      case ISSUER_INVALID:
        return TXT_10;

      case ID_INVALID:
        return TXT_11;

      case SUBJECT_INVALID:
        return TXT_12;

      case NOT_ACTIVE:
        return TXT_13;

      default:
        return TXT_4;
    }
  }

  getFieldMessages() {
    let fieldMessages = {};

    if (this.errorName === BAD_REQUEST) {
      for (let i=0; i<this.httpException.message.length; i++) {
        const item = this.httpException.message[i];
        fieldMessages[item.property] = Object.keys(item.constraints).map((key) => item.constraints[key]);
      }
    }

    return fieldMessages;
  }
}
