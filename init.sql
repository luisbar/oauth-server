create database oauth_server_db;
use oauth_server_db;

create table scope (
  id int(11) not null auto_increment,
  controller varchar(30) not null,
  handler varchar(30) not null,
  PRIMARY KEY (id)
);

create table user (
  id int(11) not null auto_increment,
  username varchar(30) not null,
  password varchar(30) not null,
  PRIMARY KEY (id)
);

create table user_scope (
  userId int(11) not null,
  scopeId int(11) not null,
  PRIMARY KEY (userId,scopeId),
  FOREIGN KEY (scopeId) REFERENCES scope (id) ON DELETE CASCADE ON UPDATE NO ACTION,
  FOREIGN KEY (userId) REFERENCES user (id) ON DELETE CASCADE ON UPDATE NO ACTION
);

insert into user (username, password) values ('luisbar', 'huachinango');

insert into scope (controller, handler) values ('authorization', 'authorize');

insert into scope (controller, handler) values ('scope', 'create');
insert into scope (controller, handler) values ('scope', 'delete');
insert into scope (controller, handler) values ('scope', 'findAll');

insert into scope (controller, handler) values ('user', 'upsert');
insert into scope (controller, handler) values ('user', 'delete');
insert into scope (controller, handler) values ('user', 'findAll');
insert into scope (controller, handler) values ('user', 'findById');
insert into scope (controller, handler) values ('user', 'signIn');
insert into scope (controller, handler) values ('user', 'revokeToken');

insert into user_scope (userId, scopeId) values (1, 1);
insert into user_scope (userId, scopeId) values (1, 2);
insert into user_scope (userId, scopeId) values (1, 3);
insert into user_scope (userId, scopeId) values (1, 4);
insert into user_scope (userId, scopeId) values (1, 5);
insert into user_scope (userId, scopeId) values (1, 6);
insert into user_scope (userId, scopeId) values (1, 7);
insert into user_scope (userId, scopeId) values (1, 8);
insert into user_scope (userId, scopeId) values (1, 9);
insert into user_scope (userId, scopeId) values (1, 10);