#### How to run

###### Using docker (with seed data)

- Build the image for the database
```sh
docker build --pull --rm -f "Database" -t oauth-server-db "."
```

- Create a network
```sh
docker network create --subnet 172.26.0.0/24 --gateway 172.26.0.1 middleware
```
:warning: If you get the following error: `Error response from daemon: Pool overlaps with other one on this address space`, change the IP

- Run the database image
```sh
docker run -d --network middleware --ip 172.26.0.2 oauth-server-db
```
:warning: If you have changed the IP, please change above too

- Create a env.ts file into `src/config` folder taking as example the env.example.ts

- Change the `host` in `ormconfig.json` file by 172.26.0.2 (or your IP)
:warning: If you have changed the IP, please change above too

- Build the image for the API
```sh
docker build --pull --rm -f "Api" -t oauth-server-api "."
```
:warning: Remove the node_modules and package-lock.json if you have them

- Run the api image
```sh
docker run -d -p 3001:3001 --network middleware oauth-server-api
```

Service will be running on http://localhost:3001

:warning: For trying this service, please run the following client https://gitlab.com/oauth2.0/client

:warning: It has seed data

###### Normal way
```sh
npm i && npm run start:dev
```

:warning: It has not seed data